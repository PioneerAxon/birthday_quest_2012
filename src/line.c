#include <line.h>

Line* line_new (const char* string)
{
	Line* new;
	new = (Line*) malloc (sizeof (Line));
	assert (new != NULL);
	new->count = 1;
	strncpy (new->string, string, LINE_MAX);
	new->string [LINE_MAX - 1] = '\0';
	return new;
}

void line_incr (Line* line)
{
	if (line)
		line->count++;
}

int line_decr (Line* line)
{
	if (line)
	{
		line->count--;
		return line->count;
	}
	return 0;
}

void line_destroy (Line* line)
{
	if (line)
		free (line);
}
