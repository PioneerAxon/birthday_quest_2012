#ifndef __LINE_COLLECTION_H__
#define __LINE_COLLECTION_H__

#include <line.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

typedef struct
{
	Line** lines;
	int line_count;
	int max_len;
} LineCollection;

LineCollection* line_collection_new ();

void line_collection_insert (LineCollection* line_collection, const char* string);

void line_collection_remove (LineCollection* line_collection, const char* string);

void line_collection_destroy (LineCollection* line_collection);

#endif
