#ifndef __IMPORTER_H__
#define __IMPORTER_H__

#include <line.h>
#include <line_collection.h>
#include <string_builder.h>

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

typedef struct
{
	FILE* file;
	LineCollection* line_collection;
} Importer;

Importer* importer_new (FILE* file);

LineCollection* importer_import_all (Importer* importer);

void importer_destroy (Importer* importer);

#endif
