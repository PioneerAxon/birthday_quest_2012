#include <importer.h>

Importer* importer_new (FILE* file)
{
	Importer* new;
	new = (Importer*) malloc (sizeof (Importer));
	assert (new);
	new->file = file;
	new->line_collection = line_collection_new ();
	return new;
}

static char* _importer_import_line (Importer* importer)
{
	int ch;
	char* ret;
	StringBuilder* sb = string_builder_new ();
	while (1)
	{
		ch = fgetc (importer->file);
		if (ch == EOF)
		{
			string_builder_destroy (sb);
			return NULL;
		}
		if (ch == '\n')
		{
			if (string_builder_length (sb) == 0)
				string_builder_insert_char (sb, '_');
			ret = string_builder_to_string (sb);
			string_builder_destroy (sb);
			return ret;
		}
		if (string_builder_length (sb) == 0)
		{
			if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == '_')
			{
				string_builder_insert_char (sb, ch);
			}
			else
			{
				string_builder_insert_char (sb, '_');
				if (ch >= '0' && ch <= '9')
					string_builder_insert_char (sb, ch);
			}
		}
		else
		{
			if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '_')
			{
				string_builder_insert_char (sb, ch);
			}
			else
			{
				string_builder_insert_char (sb, '_');
			}
		}
	}
	return NULL;
}

LineCollection* importer_import_all (Importer* importer)
{
	char* line;
	while ((line = _importer_import_line (importer)) != NULL)
	{
		line_collection_insert (importer->line_collection, line);
	}
	return importer->line_collection;
}

void importer_destroy (Importer* importer)
{
	if (!importer)
		return;
	line_collection_destroy (importer->line_collection);
	free (importer);
}
