#include <string_builder.h>

StringBuilder* string_builder_new ()
{
	StringBuilder* new;
	new = (StringBuilder*) malloc (sizeof (StringBuilder));
	assert (new);
	new->string = NULL;
	new->length = 0;
	return new;
}

void string_builder_insert_char (StringBuilder* string_builder, char ch)
{
	if (string_builder->length % STRING_SIZE == 0)
	{
		string_builder->string = (char*) realloc (string_builder->string, (string_builder->length + STRING_SIZE) * sizeof (char));
	}
	string_builder->string [string_builder->length++] = ch;
}

char* string_builder_to_string (StringBuilder* string_builder)
{
	char* ret;
	ret = (char*) malloc (sizeof (char) * (string_builder->length + 1));
	assert (ret);
	strncpy (ret, string_builder->string, string_builder->length);
	ret [string_builder->length] = '\0';
	return ret;
}

int string_builder_length (StringBuilder* string_builder)
{
	if (string_builder)
		return string_builder->length;
	return -1;
}

void string_builder_destroy (StringBuilder* string_builder)
{
	if (!string_builder)
		return;
	if (string_builder->string)
		free (string_builder->string);
	free (string_builder);
}
