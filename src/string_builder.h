#ifndef __STRING_BUILDER_H__
#define __STRING_BUILDER_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define STRING_SIZE 1024

typedef struct
{
	char* string;
	int length;
} StringBuilder;

StringBuilder* string_builder_new ();

void string_builder_insert_char (StringBuilder* string_builder, char ch);

char* string_builder_to_string (StringBuilder* string_builder);

int string_builder_length (StringBuilder* string_builder);

void string_builder_destroy (StringBuilder* string_builder);

#endif
