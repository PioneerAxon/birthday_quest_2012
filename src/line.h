#ifndef __LINE_H__
#define __LINE_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#define LINE_MAX 256

typedef struct
{
	char string [LINE_MAX];
	int count;
} Line;

Line* line_new (const char* string);

void line_incr (Line* line);

int line_decr (Line* line);

void line_destroy (Line* line);

#endif
