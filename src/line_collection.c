#include <line_collection.h>

LineCollection* line_collection_new ()
{
	LineCollection* new;
	new = (LineCollection*) malloc (sizeof (LineCollection));
	assert (new);
	new->lines = NULL;
	new->line_count = 0;
	new->max_len = 0;
	return new;
}

void line_collection_insert (LineCollection* line_collection, const char* string)
{
	int index;
	for (index = 0; index < line_collection->line_count; index++)
	{
		if (strcmp (string, line_collection->lines [index]->string) == 0)
			break;
	}
	if (index == line_collection->line_count)
	{
		// ERROR: String too long for a function name. Ignore it. :P
		if (strlen (string) > LINE_MAX)
			return;
		if (strlen (string) > line_collection->max_len)
			line_collection->max_len = strlen (string);
		line_collection->lines = (Line**) realloc (line_collection->lines, sizeof (Line*) * (index + 1));
		line_collection->lines [index] = line_new (string);
		line_collection->line_count++;
	}
	else
	{
		line_incr (line_collection->lines [index]);
	}
}

void line_collection_remove (LineCollection* line_collection, const char* string)
{
	int index;
	for (index = 0; index < line_collection->line_count; index++)
	{
		if (strcmp (string, line_collection->lines [index]->string) == 0)
			break;
	}
	if (index == line_collection->line_count)
	{
		/* WARNING: This should never happen. */
		return;
	}
	if (line_decr (line_collection->lines [index]) == 0)
	{
		line_destroy (line_collection->lines [index]);
		line_collection->line_count--;
		line_collection->lines [index] = line_collection->lines [line_collection->line_count];
		line_collection->lines = (Line**) realloc (line_collection->lines, line_collection->line_count * sizeof (Line*));
	}
}

void line_collection_destroy (LineCollection* line_collection)
{
	if (!line_collection)
		return;
	while (line_collection->line_count--)
	{
		line_destroy (line_collection->lines [line_collection->line_count]);
	}
	free (line_collection);
}
